
mp.events.addCommand('promo', (player, code) =>
{
    DB.Handle.query("SELECT * FROM promocodes WHERE code=?", [code], (e, resultp1) => {
        if (resultp1.length === 0) return player.utils.error(`Ошибка использования промо-кода - код не найден!`);
        if (resultp1[0].character_id === player.sqlId) return player.utils.error(`Вы не можете использовать свой промокод!`);
        DB.Handle.query(`SELECT * FROM promocodes_data WHERE regId=? and idcode=?`, [player.sqlId, resultp1[0].id], (e, resultp2) => {
            if (0 < resultp2.length) return player.utils.error(`Вы уже использовали данный промо-код!`);
            const rewarded = `${player.name} выиграл 5000 $`;
            var promoVals = [resultp1[0].id,player.sqlId,JSON.stringify(rewarded)];
            DB.Handle.query(`INSERT INTO promocodes_data (idcode,regId,date,rewarded) VALUES (?,?,NOW(),?)`, promoVals, () => {
            });
            DB.Handle.query("SELECT name FROM characters WHERE id=?", [player.sqlId], (e, resultp3) => {
                if (resultp3.length === 0) return player.utils.error(`Ошибка использования промо-кода - пользователь кода не найден!`);
                player.utils.setMoney(player.money + 5000);
                DB.Handle.query("UPDATE promocodes SET reward_sum = ? WHERE character_id = ?", [player.money, player.sqlId]);
                player.utils.success(`Вам начислены 5000 $ !`);
            });
            let idCodeCreator = resultp1[0]['character_id'];
            let codeCreator = getPlayerById(idCodeCreator);
            if (codeCreator !== null)  codeCreator.utils.setMoney(player.money + 3000);
            else  DB.Handle.query("SELECT money FROM characters WHERE id=?", [idCodeCreator], (e, resultp4) => {
                let moneyCreator = resultp4[0]['money'];
                moneyCreator += 3000;
                DB.Handle.query("UPDATE characters SET money = ? WHERE id = ?", [moneyCreator, idCodeCreator]);
                DB.Handle.query("UPDATE promocodes SET reward_sum = ? WHERE character_id = ?", [moneyCreator, idCodeCreator]);
            });
            });
    });
});

function getPlayerById(id) {
    const players = mp.players.toArray();
    for (const player of players) {
        if (player.id === id) return player;
    }
    return null;
}