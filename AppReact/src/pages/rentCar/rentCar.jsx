import React, { Component } from "react";
import rage from "../../helpers/ragemp";
import "./css/rentCar.css";

export default class RentCar extends Component {
  state = {
    show: true,
    time: 1,
    selected: 0,
    color: 0,
    vehicles: [
      { name: "car1", title: "Chrisler",    price: 390 },
      { name: "car2", title: "Aston",       price: 490 },
      { name: "car3", title: "BMW",         price: 690 },
      { name: "car3", title: "Rolls Royce", price: 1100 },
    ]
  };

  colors = [ "a34040", "a5c93e", "f72424", "42cf50", "5382c8", "f3f4a0" ];

  updateCar = (selected) => this.setState({ selected });
  updateColor = (color) => this.setState({ color });
  updateTime = (e) => this.setState({ time: e.target.value });

  rent = (isBank) => { rage.CallClient('rent_car', isBank, this.state.selected, this.state.color, this.state.time) }; //функция которая вызывается при клике на кнопку оплаты

  timeText = () => {
    const time = this.state.time;
    switch (true) {
        case time > 4:
            return time + " часов";
        case time > 1:
            return time + " часа";
        default:
            return time + " час";
    }
  }

  componentDidMount(){
    window.rentcar = {
      enable: (show) => this.setState({show}),
      active: () => this.state.show,
      vehicles: (vehicles) => this.setState({vehicles}),
    }
  }

  render() {
    const { show, color, time, selected, vehicles } = this.state;

    return show && (
      <div className="rent">
        <div className="rent__list">
            {vehicles.map((item, index) => (
            <div
                key={index}
                className={`rent__list__car ${selected === index ? "active" : ""}`}
                style={{ background: `var(--background) url(${require(`./img/${item.name}.png`)})` }}
                onClick={() => this.updateCar(index)}
            >
                <span>${item.price}/час</span>
            </div>
            ))}
        </div>
        <div className="rent__control">
            <div className="rent__control__name">
                {vehicles[selected].title}
            </div>
            <div className="rent__control__colors">
            {this.colors.map((item, index) => 
                <div
                    onClick={() => this.updateColor(index)}
                    style={{ background: `#${item}` }}
                    className={color === index ?"rent__control__color__active":""}
                />
            )}
            </div>
            <div className="rent__control__price">
                <div className="rent__control__sum">
                    ${time * vehicles[selected].price}
                </div>
                <div className="rent__control__time">
                    {this.timeText()}
                </div>
            </div>
            <input className="rent__control__range" type="range" min="1" max="8" value={time} onChange={(e) => this.updateTime(e)} />
            <div className="rent__control__buttons">
                <div className="rent__control__bank" onClick={() => this.rent(true)}>
                    <div className="rent__control__bank__img"/>
                    <span>Карта</span>
                </div>
                <div className="rent__control__cash" onClick={() => this.rent(false)}>
                    <div className="rent__control__cash__img"/>
                    <span>Наличные</span>
                </div>
            </div>
        </div>
      </div>
    );
  }
}
