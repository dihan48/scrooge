import React from 'react';
import rage from "../../helpers/ragemp";// обёртка для mp.trigger и mp.invoke
import './css/dialoguebot.css';

export default class DialogueBot extends React.Component {
  state = {
    show: false,
    bot_name: 'Главный разнорабочий',
    bot_text: 'Ты неплохо потрудился! Вот тебе награда - забирай! \n \n Что дальше будет делать? Еще меня есть для тебя несколько заданий, если будет время - заглядывай!',
    variants: [
      { text: 'Круто, я с удовольствием еще загляну!', quest: true, action: rage.CallClient('event_name', '...args')},// action - функция которая вызывается при клике на вариант
      { text: 'Что ты знаешь о мафии?', quest: false, action: null },
      { text: 'Уйти', quest: false, action: null },
    ],
  };

  componentDidMount(){
    window.botdialog = {
      enable: (show) => this.setState({show}),
      active: () => this.state.show,
      name: (bot_name) => this.setState({bot_name}),
      question: (bot_text) => this.setState({bot_text}),
      responses: (variants) => this.setState({variants}),
    }
  }

  render() {
    return this.state.show && (
      <div className='main_dialog'>
        <div className='dialog_left'>
          {this.state.variants.map((e, i) => 
              <span key={i} className={e.quest && 'gold_hover'}  onClick={e.action}>
                {e.text}
              </span>
            )}
        </div>
        <div className='dialog_right'>
          <div className='d_r_t'>{this.state.bot_name}</div>
          <div className='d_r_sp'>{this.state.bot_text}</div>
        </div>
      </div>
    );
  }
}
