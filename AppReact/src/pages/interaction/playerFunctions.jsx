
/* Добавляем/удаляем пункты, в зависимости от данных. */
export function addictivePlayerItems(data) {
    if (!data) data = {};
    if (data.action == "showDocuments") {
        $("#interactionMenu").empty();
        var items = [{
            text: "Паспорт",
            icon: "card.png"
        },
            {
                text: "Лицензии",
                icon: "card.png"
            },
            {
                text: "Выписка из тира",
                icon: "card.png"
            },
            {
                text: "Трудовая",
                icon: "card.png"
            },
        ];
        var factions = [2, 3, 4, 5, 6, 7]; // организации с удостоверениями
        if (factions.indexOf(clientStorage.faction) != -1) items.push({
            text: "Удостоверение",
            icon: "card.png"
        });
        for (var i = 0; i < items.length; i++) {
            var info = items[i];
            var iconName = (info.icon) ? info.icon : "default.png";
            interactionMenuAPI.addItem(iconName, info.text);
        }
    } else if (data.action == "showLeader") {
        $("#interactionMenu").empty();
        var items = [{
            text: "Пригласить в организацию",
            icon: "default.png"
        },
            {
                text: "Повысить в должности",
                icon: "default.png"
            },
            {
                text: "Понизить в должности",
                icon: "default.png"
            },
            {
                text: "Уволить из организации",
                icon: "default.png"
            },
        ];
        for (var i = 0; i < items.length; i++) {
            var info = items[i];
            var iconName = (info.icon) ? info.icon : "default.png";
            interactionMenuAPI.addItem(iconName, info.text);
        }
    } else if (data.action == "showFaction") {
        $("#interactionMenu").empty();
        var list = {
            '1': [{
                text: "todo",
                icon: "default.png"
            }, ],
            '2': [{
                text: "Наручники",
                icon: "default.png"
            },
                {
                    text: "Розыск",
                    icon: "default.png"
                },
                {
                    text: "Вести за собой",
                    icon: "default.png"
                },
                {
                    text: "Посадить в авто",
                    icon: "default.png"
                },
                {
                    text: "Арестовать",
                    icon: "default.png"
                },
                {
                    text: "Штраф",
                    icon: "default.png"
                },
            ],
            '3': [{
                text: "Наручники",
                icon: "default.png"
            },
                {
                    text: "Розыск",
                    icon: "default.png"
                },
                {
                    text: "Вести за собой",
                    icon: "default.png"
                },
                {
                    text: "Посадить в авто",
                    icon: "default.png"
                },
                {
                    text: "Арестовать",
                    icon: "default.png"
                },
                {
                    text: "Штраф",
                    icon: "default.png"
                },
            ],
            '4': [{
                text: "Наручники",
                icon: "default.png"
            },
                {
                    text: "Розыск",
                    icon: "default.png"
                },
                {
                    text: "Вести за собой",
                    icon: "default.png"
                },
                {
                    text: "Посадить в авто",
                    icon: "default.png"
                },
                {
                    text: "Арестовать",
                    icon: "default.png"
                },
                // {
                //     text: "Прикрепить жучок",
                //     icon: "default.png"
                // },
                // {
                //     text: "Обыскать",
                //     icon: "default.png"
                // },
            ],
            '5': [{
                text: "Вылечить",
                icon: "default.png"
            },
                {
                    text: "Показать удостоверение",
                    icon: "default.png"
                }
            ],
            '6': [{
                text: "todo",
                icon: "default.png"
            }, ],
            '7': [{
                text: "todo",
                icon: "default.png"
            }, ],
            '8': [{
                text: "todo",
                icon: "default.png"
            }, ],
            '9': [{
                text: "todo",
                icon: "default.png"
            }, ],
            '10': [{
                text: "todo",
                icon: "default.png"
            }, ],
            '11': [{
                text: "todo",
                icon: "default.png"
            }, ],
            '12': [{
                text: "todo",
                icon: "default.png"
            }, ],
            '13': [{
                text: "todo",
                icon: "default.png"
            }, ],
            '14': [{
                text: "todo",
                icon: "default.png"
            }, ],
            '15': [{
                text: "todo",
                icon: "default.png"
            }, ],
            '16': [{
                text: "todo",
                icon: "default.png"
            }, ],
            '17': [{
                text: "todo",
                icon: "default.png"
            }, ],
        };
        if (list[clientStorage.faction]) {
            var items = list[clientStorage.faction];
            for (var i = 0; i < items.length; i++) {
                var info = items[i];
                var iconName = (info.icon) ? info.icon : "default.png";
                interactionMenuAPI.addItem(iconName, info.text);
            }
        }
    } else if (data.action == "showLocal") {
        $("#interactionMenu").empty();
        var items = [{
            text: "Эмоции",
            icon: "default.png"
        },
            {
                text: "Походка",
                icon: "default.png"
            },
            {
                text: "Анимации",
                icon: "default.png"
            },
        ];
        for (var i = 0; i < items.length; i++) {
            var info = items[i];
            var iconName = (info.icon) ? info.icon : "default.png";
            interactionMenuAPI.addItem(iconName, info.text);
        }
    } else if (data.action == "showEmotions") {
        $("#interactionMenu").empty();
        var items = [{
            text: "Обычный",
            icon: "default.png"
        },
            {
                text: "Угрюмый",
                icon: "default.png"
            },
            {
                text: "Сердитый",
                icon: "default.png"
            },
            {
                text: "Счастливый",
                icon: "default.png"
            },
            {
                text: "Стресс",
                icon: "default.png"
            },
            {
                text: "Надутый",
                icon: "default.png"
            },
        ];
        for (var i = 0; i < items.length; i++) {
            var info = items[i];
            var iconName = (info.icon) ? info.icon : "default.png";
            interactionMenuAPI.addItem(iconName, info.text);
        }
    } else if (data.action == "showWalking") {
        $("#interactionMenu").empty();
        var items = [{
            text: "Нормальная",
            icon: "default.png"
        },
            {
                text: "Храбрый",
                icon: "default.png"
            },
            {
                text: "Уверенный",
                icon: "default.png"
            },
            {
                text: "Гангстер",
                icon: "default.png"
            },
            {
                text: "Быстрый",
                icon: "default.png"
            },
            {
                text: "Грустный",
                icon: "default.png"
            },
            {
                text: "Крылатый",
                icon: "default.png"
            },
        ];
        for (var i = 0; i < items.length; i++) {
            var info = items[i];
            var iconName = (info.icon) ? info.icon : "default.png";
            interactionMenuAPI.addItem(iconName, info.text);
        }
    } else if (data.action == "showAnimations") {
        $("#interactionMenu").empty();
        
        var list = {
            '-1': [{
                text: "Жесты",
                icon: "default.png"
            },
                {
                    text: "Спорт",
                    icon: "default.png"
                },
                {
                    text: "Поза лёжа",
                    icon: "default.png"
                },
                {
                    text: "Поза сидя",
                    icon: "default.png"
                },
                {
                    text: "Испугано",
                    icon: "default.png"
                },
                {
                    text: "Танец",
                    icon: "default.png"
                },
                {
                    text: "Курение",
                    icon: "default.png"
                },
            ],
            '0': [{
                text: "Показывает фак",
                icon: "default.png"
            },
                {
                    text: "Рок двумя руками",
                    icon: "default.png"
                },
                {
                    text: "Рок одной рукой",
                    icon: "default.png"
                },
                {
                    text: "Два пальца",
                    icon: "default.png"
                },
                {
                    text: "Бурные аплодисменты",
                    icon: "default.png"
                },
                {
                    text: "Анимация дрочки",
                    icon: "default.png"
                },
            ],
            '1': [{
                text: "Подтягивания",
                icon: "default.png"
            },
                {
                    text: "Тягает штангу лёжа",
                    icon: "default.png"
                },
                {
                    text: "Тягает штангу стоя",
                    icon: "default.png"
                },
                {
                    text: "Отжимания",
                    icon: "default.png"
                },
                {
                    text: "Качает пресс",
                    icon: "default.png"
                },
            ],
            '2': [{
                text: "Спит",
                icon: "default.png"
            },
                {
                    text: "Спит развалив руки",
                    icon: "default.png"
                },
                {
                    text: "Спит сжавшись в клубок",
                    icon: "default.png"
                },
            ],
            '3': [{
                text: "Скорчился в клубок",
                icon: "default.png"
            },
                {
                    text: "Сидит разглядывая что-то",
                    icon: "default.png"
                },
                {
                    text: "Сидит со стаканом и пьёт",
                    icon: "default.png"
                },
                {
                    text: "Сидит свободно раздвинув ноги",
                    icon: "default.png"
                },
                {
                    text: "Сидит рука на руке",
                    icon: "default.png"
                },
            ],
            '4': [{
                text: "Стоя закрывается руками",
                icon: "default.png"
            },
                {
                    text: "Закрывается руками и осм.",
                    icon: "default.png"
                },
                {
                    text: "Нагнувшись закрывается руками",
                    icon: "default.png"
                },
                {
                    text: "Нагнувшись осм. по сторонам",
                    icon: "default.png"
                },
                {
                    text: "Сжимается в клубок",
                    icon: "default.png"
                },
                {
                    text: "Сидя закрывается руками осм.",
                    icon: "default.png"
                },
                {
                    text: "Оглядывается по сторонам и трясётся",
                    icon: "default.png"
                },
                {
                    text: "Сидя на картах закрывшись кулаками",
                    icon: "default.png"
                },
                {
                    text: "Сидя на картах испугано злится",
                    icon: "default.png"
                },
            ],
            '5': [{
                text: "Трясёт руками",
                icon: "default.png"
            },
                {
                    text: "Облакотился на стену",
                    icon: "default.png"
                },
                {
                    text: "Танцует тряся тазом",
                    icon: "default.png"
                },
            ],
            '6': [{
                text: "Берёт в руки сигарету",
                icon: "default.png"
            },
            ],
        };
        
        if (list[data.index]) {
            var items = list[data.index];
            for (var i = 0; i < items.length; i++) {
                var info = items[i];
                var iconName = (info.icon) ? info.icon : "default.png";
                interactionMenuAPI.addItem(iconName, info.text);
            }
        }
    } else {
        if (clientStorage.faction > 0) {
            interactionMenuAPI.addItem("default.png", "Организация");
        }
        if (clientStorage.factionRank >= clientStorage.factionLastRank) {
            interactionMenuAPI.addItem("default.png", "Лидер");
        }
        if (clientStorage.trashLeader === 1) {
            interactionMenuAPI.addItem("default.png", "Пригласить в бригаду");
        }
        if (clientStorage.trashLeader === 2) {
            interactionMenuAPI.addItem("default.png", "Уволить из бригады");
        }
        if (clientStorage.gopostalLeader === 1) {
            interactionMenuAPI.addItem("default.png", "Пригласить в группу");
        }
        if (clientStorage.gopostalLeader === 2) {
            interactionMenuAPI.addItem("default.png", "Уволить из группы");
        }
        if (data.showTransferProducts) {
            interactionMenuAPI.addItem("default.png", "Передать товар");
        }
    }
}


/* Установка обработчиков на пункты меню взаимодействия с игроком. */
export function initPlayerItemsHandler() {
    $("#interactionMenu .interaction_item").each((index, el) => {
        $(el).click((e) => {
            onClickPlayerItem($(el).find(".text").text());
        });
    });
}


/* Вызывается при клике на меню взаимодействия с игроком. */
function onClickPlayerItem(itemName) {
//     let action = "";
//     let index = -100;
//     switch (itemName) {
//         case "Показать документы": action = "showDocuments";              break;
//         case "Лидер":              action = "showLeader";                 break;
//         case "Организация":        action = "showFaction";                break;
//         case "Эмоции":             action = "showEmotions";               break;
//         case "Походка":            action = "showWalking";                break;
//         case "Анимации":           action = "showAnimations"; index = -1; break;
//         case "Жесты":              action = "showAnimations"; index =  0; break;
//         case "Спорт":              action = "showAnimations"; index =  1; break;
//         case "Поза лёжа":          action = "showAnimations"; index =  2; break;
//         case "Поза сидя":          action = "showAnimations"; index =  3; break;
//         case "Испугано":           action = "showAnimations"; index =  4; break;
//         case "Танец":              action = "showAnimations"; index =  5; break;
//         case "Курение":            action = "showAnimations"; index =  6; break;
//         default: break;
//     }
//     if (index === -100)  {
//         interactionMenuAPI.showPlayerMenu(JSON.stringify({
//             action: action
//         }));
//         return;
//     } else {
//         interactionMenuAPI.showPlayerMenu(JSON.stringify({
//             action: action,
//             index:  index
//         }));
//         return;
//     }
    
    if (itemName == "Показать документы") {
        interactionMenuAPI.showPlayerMenu(JSON.stringify({
            action: "showDocuments"
        }));
        return;
    } else if (itemName == "Лидер") {
        interactionMenuAPI.showPlayerMenu(JSON.stringify({
            action: "showLeader"
        }));
        return;
    } else if (itemName == "Организация") {
        interactionMenuAPI.showPlayerMenu(JSON.stringify({
            action: "showFaction"
        }));
        return;
    } else if (itemName == "Эмоции") {
        interactionMenuAPI.showPlayerMenu(JSON.stringify({
            action: "showEmotions"
        }));
        return;
    } else if (itemName == "Походка") {
        interactionMenuAPI.showPlayerMenu(JSON.stringify({
            action: "showWalking"
        }));
        return;
    } else if (itemName == "Анимации") {
        interactionMenuAPI.showPlayerMenu(JSON.stringify({
            action: "showAnimations",
            index: -1
        }));
        return;
    } else if (itemName == "Жесты") {
        interactionMenuAPI.showPlayerMenu(JSON.stringify({
            action: "showAnimations",
            index: 0
        }));
        return;
    } else if (itemName == "Спорт") {
        interactionMenuAPI.showPlayerMenu(JSON.stringify({
            action: "showAnimations",
            index: 1
        }));
        return;
    } else if (itemName == "Поза лёжа") {
        interactionMenuAPI.showPlayerMenu(JSON.stringify({
            action: "showAnimations",
            index: 2
        }));
        return;
    } else if (itemName == "Поза сидя") {
        interactionMenuAPI.showPlayerMenu(JSON.stringify({
            action: "showAnimations",
            index: 3
        }));
        return;
    } else if (itemName == "Испугано") {
        interactionMenuAPI.showPlayerMenu(JSON.stringify({
            action: "showAnimations",
            index: 4
        }));
        return;
    } else if (itemName == "Танец") {
        interactionMenuAPI.showPlayerMenu(JSON.stringify({
            action: "showAnimations",
            index: 5
        }));
        return;
    } else if (itemName == "Курение") {
        interactionMenuAPI.showPlayerMenu(JSON.stringify({
            action: "showAnimations",
            index: 6
        }));
        return;
    }
    mp.trigger(`interactionMenu.onClickPlayerItem`, itemName);
}