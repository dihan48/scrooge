
let lastAction = null;

/* Добавляем/удаляем пункты, в зависимости от данных. */
export function addictiveVehicleItems(data) {
    if (!data) data = {};
    //debug(`data: ${JSON.stringify(data)}`)
    if (data.action == "showFaction") {
        $("#interactionMenu").empty();
        var list = {
            '1': [{
                text: "todo",
                icon: "default.png"
            }, ],
            '2': [{
                text: "Вскрыть",
                icon: "default.png"
            },
                {
                    text: "Вытолкнуть",
                    icon: "default.png"
                },
            ],
            '3': [{
                text: "Вскрыть",
                icon: "default.png"
            },
                {
                    text: "Вытолкнуть",
                    icon: "default.png"
                },
            ],
            '4': [{
                text: "todo",
                icon: "default.png"
            }, ],
            '5': [{
                text: "todo",
                icon: "default.png"
            }, ],
            '6': [{
                text: "todo",
                icon: "default.png"
            }, ],
            '7': [{
                text: "todo",
                icon: "default.png"
            }, ],
            '8': [{
                text: "todo",
                icon: "default.png"
            }, ],
            '9': [{
                text: "todo",
                icon: "default.png"
            }, ],
            '10': [{
                text: "todo",
                icon: "default.png"
            }, ],
            '11': [{
                text: "todo",
                icon: "default.png"
            }, ],
            '12': [{
                text: "todo",
                icon: "default.png"
            }, ],
            '13': [{
                text: "todo",
                icon: "default.png"
            }, ],
            '14': [{
                text: "todo",
                icon: "default.png"
            }, ],
            '15': [{
                text: "todo",
                icon: "default.png"
            }, ],
            '16': [{
                text: "todo",
                icon: "default.png"
            }, ],
            '17': [{
                text: "todo",
                icon: "default.png"
            }, ],
        };
        if (list[clientStorage.faction]) {
            var items = list[clientStorage.faction];
            for (var i = 0; i < items.length; i++) {
                var info = items[i];
                var iconName = (info.icon) ? info.icon : "default.png";
                interactionMenuAPI.addItem(iconName, info.text);
            }
        }
    } else if (data.action == "removeFromVehicle") {
        $("#interactionMenu").empty();
        for (var i = 0; i < data.names.length; i++) {
            var name = data.names[i];
            var iconName = "default.png";
            interactionMenuAPI.addItem(iconName, name);
        }
        lastAction = "removeFromVehicle";
    } else if (clientStorage.faction > 0) {
        interactionMenuAPI.addItem("default.png", "Организация");
    }
    
    if (data.action == "showDoors") {
        interactionMenuAPI.addBeforeItem("default.png", "Двери");
    } else if (data.action == "showHood") {
        interactionMenuAPI.addBeforeItem("default.png", "Капот");
    } else if (data.action == "showBoot") {
        interactionMenuAPI.addBeforeItem("default.png", "Багажник");
        if (data.showProducts) interactionMenuAPI.addItem("default.png", "Товар");
    } else if (data.action == "showEnter") {
        $("#interactionMenu").empty();
        interactionMenuAPI.addItem("default.png", "Выкинуть из транспорта");
        interactionMenuAPI.addItem("default.png", "Открыть/Закрыть транспорт");
        // interactionMenuAPI.addItem("default.png", "Открыть/Закрыть капот");
        // interactionMenuAPI.addItem("default.png", "Открыть/Закрыть багажник");
    }
}


/* Установка обработчиков на пункты меню взаимодействия с авто. */
export function initVehicleItemsHandler() {
    $("#interactionMenu .interaction_item").each((index, el) => {
        $(el).click((e) => {
            onClickVehicleItem($(el).find(".text").text());
        });
    });
}


/* Вызывается при клике на меню взаимодействия с авто. */
function onClickVehicleItem(itemName) {
    if (itemName == "Организация") {
        interactionMenuAPI.showVehicleMenu(JSON.stringify({
            action: "showFaction"
        }));
        return;
    } else if (lastAction == "removeFromVehicle") {
        mp.trigger(`events.callRemote`, `removeFromVehicle`, itemName);
        interactionMenuAPI.hide();
    }
    mp.trigger(`interactionMenu.onClickVehicleItem`, itemName);
}


export function lastActionNull() {
    lastAction = null;
}