import React from 'react';
import { Router } from 'react-router-dom';
import { connect } from 'react-redux';

import { history } from '../helpers/history';

import { MedicTablet } from '../pages/tablets/MedicTablet';
import { FibTablet } from '../pages/tablets/FibTablet';
import { ArmyTablet } from '../pages/tablets/ArmyTablet';
import { PdTablet } from '../pages/tablets/PdTablet';
import { PlayerMenu } from '../pages/PlayerMenu';
import { BuyCar } from '../pages/autoSaloon/BuyCar';
import { SheriffTablet } from '../pages/tablets/SheriffTablet';
import { BandTablet } from '../pages/tablets/BandTablet';
import { MedicCerf } from '../pages/cerfs/MedicCerf';
import { SheriffCerf } from '../pages/cerfs/SheriffCerf';
import { SpeedoMeter } from '../pages/carSystem/SpeedoMeter';
import { SpecMenu } from '../pages/carSystem/SpecMenu';
import { Business } from '../pages/Business';
import { Hud } from '../pages/Hud';
import { AtmMenu } from '../pages/AtmMenu';
import { TelePhone } from '../pages/TelePhone';
import {Interaction} from "./interaction/interaction";

import  DialogBot from '../pages/dialogBot/DialogueBot';
import  RentCar from '../pages/rentCAr/rentCar';
//import { PlayerInventory } from '../pages/inventory/playerInventory';

class App extends React.Component {
    constructor(props) {
        super(props);
    }
 
    render() {
        return (
            <Router history={history}> 
                <React.Fragment>
                    <MedicTablet />
                    <ArmyTablet />
                    <PdTablet />
                    <FibTablet />
                    <PlayerMenu />
                    <MedicCerf />
                    <Business />
                    <SheriffCerf />
                    <SpeedoMeter />
                    <SheriffTablet />
                    <TelePhone />
                    <BandTablet/>
                    <SpecMenu/>
                    <BuyCar/>
                    <AtmMenu/>
                    <Interaction/>
                    {/*<PlayerInventory/>*/}
                    <Hud/>
                    <DialogBot/>
                    <RentCar/>
                </React.Fragment>
            </Router>
        );
    }
}

       
function mapStateToProps(state) {
    const { alert } = state;
    return {
        alert
    };
}


const connectedApp = connect(mapStateToProps)(App);
export { connectedApp as App }; 