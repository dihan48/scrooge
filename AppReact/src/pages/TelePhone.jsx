import React from 'react';
import { connect } from 'react-redux';
import ReactDOM from 'react-dom';
import Clock from 'react-live-clock';
import '../assets/css/telePhone.css';

class TelePhone extends React.Component { 
    constructor(props) {
        super(props);
        this.state = {
            phoneStatus: false,
            slideWidth: 0,
            slideHeight: 0,
            sliderUlWidth: 0,
            slide: 0,
            page: 0,
            step: 0,
            style: 0,
            inComingMessages: [],
            outComingMessages: [],
            contacts: [],
            selectId: 0,
            selectIdContact: 0,
            telephone_number: false,
            contactName: undefined,
            contactNumber: undefined,
            message: undefined
        };

        this.changeState = this.changeState.bind(this);
        this.moveLeft = this.moveLeft.bind(this);
        this.moveRight = this.moveRight.bind(this);
        this.selectId = this.selectId.bind(this);
        this.selectIdContact = this.selectIdContact.bind(this);
        this.onChange = this.onChange.bind(this);
        this.addContact = this.addContact.bind(this);
        this.sendMessage = this.sendMessage.bind(this);
        this.changeContact = this.changeContact.bind(this);
        this.deleteContact = this.deleteContact.bind(this);
        this.callContact = this.callContact.bind(this);
    };

    componentDidMount() {
        window.telephone = {
            setnum: (num) => {
                this.setState({ telephone_number: num });
            },
            enable: (enable) => {
            },
            active: () => {
                return this.state.phoneStatus;
            },
            changeOptions: (event, options) => {
                const { inComingMessages, outComingMessages, contacts } = this.state;
                if (event === 'outComingMessage') {
                    this.setState({ outComingMessages: [...outComingMessages, options] });
                    if (this.state.step === 3) this.setState({ step: 2 });
                    this.setState({ message: undefined });
                } else if (event === 'inComingMessage') {
                    this.setState({ inComingMessages: [...inComingMessages, options] });
                } else if (event === 'contacts') {
                    this.setState({ contacts: [...contacts, options] });
                    this.setState({ step: 0 });
                    this.setState({ contactName: undefined });
                    this.setState({ contactNumber: undefined });
                } else if (event === 'contact') {
                    var contact = contacts.filter(con => con.id != options.id);
                    if (this.state.step === 3 || this.state.step === 4) this.setState({ step: 2 });
                    this.setState({ contactName: undefined });
                    this.setState({ contactNumber: undefined });
                    this.setState({ contacts: [...contact, options] });
                } else if (event === 'delete') {
                    if (this.state.step === 2) this.setState({ step: 0 });
                    this.setState({ contacts: contacts.filter(con => con.id != options) });
                }
            },
        };
    };

    onChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        });
    };

    moveLeft() {
        if (this.state.slide <= 0) { 
            this.setState({ slide: 2 });
        } else {
            this.setState({ slide: this.state.slide - 1 });
        }

        document.querySelector('#slider ul').prepend(document.querySelector('#slider ul li:last-child'));
    };

    moveRight() {
        if (this.state.slide >= 2) { 
            this.setState({ slide: 0 });
        } else {
            this.setState({ slide: this.state.slide + 1 });
        }

        document.querySelector('#slider ul').append(document.querySelector('#slider ul li:first-child'));
    };

    selectId(id) {
        this.setState({ selectId: id });
        if (this.state.step === 1) return this.setState({ step: 2 });
        if (this.state.step === 3) return this.setState({ step: 4 });
    };
    
    addContact() {
        mp.trigger('select.add.contact', this.state.contactName, this.state.contactNumber);
    };

    sendMessage() {
        mp.trigger('sendMessage', this.state.selectIdContact, this.state.message);
    };

    changeContact() {
        mp.trigger('selectChangeContact', this.state.selectIdContact, this.state.contactName, this.state.contactNumber);
    };

    deleteContact() {
        mp.trigger('deleteContact', this.state.selectIdContact);
    };

    callContact(num) {
        mp.trigger('telephone.call', num);  
    };

    selectIdContact(id) {
        this.setState({ selectIdContact: id });
        this.setState({ step: 2 });
    };

    changeState(event, value) {
        if (event === 'step') {
            this.setState({ step: value });
        } else if (event === 'page') {
            if(value != 0) {
                document.querySelector('#slider').style.display = 'none';
            } else {
                document.querySelector('#slider').style.display = 'block';
            }

            this.setState({ page: value });
            this.setState({ step: 0 });
        } else if (event === 'style') {
            this.setState({ style: value });

            var bg = document.getElementById("uPhoneS");

            if (value === 1) {
                bg.style.backgroundImage = `url(${require("../assets/img/phone/bgBlue.png")})`;
                document.getElementById("messages").src = require("../assets/img/phone/messages.png");
                document.getElementById("contacts").src = require("../assets/img/phone/contacts.png");
                document.getElementById("tools").src = require("../assets/img/phone/tools.png");
                document.getElementById("arrowOne").src = require("../assets/img/phone/arrow.svg");
                document.getElementById("arrowTwo").src = require("../assets/img/phone/arrow.svg");
            } else {
                bg.style.backgroundImage = `url(${require("../assets/img/phone/bgPink.png")})`;
                document.getElementById("messages").src = require("../assets/img/phone/messagesPink.png");
                document.getElementById("contacts").src = require("../assets/img/phone/contactsPink.png");
                document.getElementById("tools").src = require("../assets/img/phone/toolsPink.png");
                document.getElementById("arrowOne").src = require("../assets/img/phone/arrowPink.svg");
                document.getElementById("arrowTwo").src = require("../assets/img/phone/arrowPink.svg");
            }
        }
    };

    render() {
        const { phoneStatus } = this.state;
        return (
            <div id="uPhoneS" className={phoneStatus === true ? 'uPhoneS' : 'uPhoneS Animate'} style={{display: 'none'}}>
                <div className="screen">
                    <div className="header">
                        <div className="time"><Clock format={'HH:mm'} ticking={true} timezone={'Europe/Moscow'} /></div>
                        <div className="day"> </div>
                    </div>
                    <div className="inScreen">
                        <div id="slider" style={{width: this.state.slideWidth, height: this.state.slideHeight, display: 'block'}}>
                            <ul style={{width: 3*this.state.slideWidth, marginLeft: -this.state.slideWidth}}>
                                <li><img tabIndex="-1" id="tools" onClick={() => this.changeState('page', 1)} src={require('../assets/img/phone/tools.png')}/></li>
                                <li><img tabIndex="-1" id="messages" onClick={() => this.changeState('page', 2)} src={require('../assets/img/phone/messages.png')}/></li>
                                <li><img tabIndex="-1" id="contacts" onClick={() => this.changeState('page', 3)} src={require('../assets/img/phone/contacts.png')}/></li>
                            </ul>
                            <a tabIndex="-1" id="arrowOne" className="control_next"><img onClick={() => this.moveRight()} src={require('../assets/img/phone/arrow.svg')}/></a>
                            <a tabIndex="-1" id="arrowTwo" className="control_prev"><img onClick={() => this.moveLeft()} src={require('../assets/img/phone/arrow.svg')}/></a>
                        </div>
                        {this.state.page === 1 ?
                            <React.Fragment>
                                {this.state.step === 0 ?
                                <div className="toolsList">
                                    <div className="headerContacts">Выберите действие</div>
                                    <div className="buttonInContent">
                                        <div className="textButtonInContent" onClick={() => this.changeState('step', 1)}>Тема</div>
                                    </div>
                                    <div className="buttonInContent">
                                        <div className="textButtonInContent">Звук</div>
                                    </div>
                                </div> : null}
                                {this.state.step === 1 ?
                                <div className="themeList">
                                    <div className="headerContacts">Выберите тему</div>
                                    <div className="buttonInContent">
                                        <div className="textButtonInContent" onClick={() => this.changeState('style', 1)}>Blue</div>
                                    </div>
                                    <div className="buttonInContent">
                                        <div className="textButtonInContent" onClick={() => this.changeState('style', 2)}>Pink</div>
                                    </div>
                                </div> : null}
                                <div className="settings">
                                    {this.state.step != 0 ? 
                                        <div className="back" onClick={() => this.changeState('step', 0)}>back</div> : 
                                        <div className="back" onClick={() => this.changeState('page', 0)}>back</div> 
                                    }
                                </div>
                            </React.Fragment>
                         : null}
                        {this.state.page === 2 ?
                            <React.Fragment>
                                {this.state.step === 0 ?
                                    <React.Fragment>
                                        <div className="messagesList">
                                            <div className="headerContacts">Выберите список</div>
                                            <div className="buttonInContent">
                                                <div className="textButtonInContent" onClick={() => this.changeState('step', 1)}>Входящие</div>
                                            </div>
                                            <div className="buttonInContent">
                                                <div className="textButtonInContent" onClick={() => this.changeState('step', 3)}>Исходящие</div>
                                            </div>
                                        </div>
                                        <div className="settings">
                                            <div className="back" onClick={() => this.changeState('page', 0)}>back</div> 
                                        </div> 
                                    </React.Fragment>
                                : null}
                                {this.state.step === 1 ?
                                    <React.Fragment>
                                        <div className="incomingMessageList">
                                            <div className="headerContacts">Входящие</div>
                                            {this.state.inComingMessages.map((item, i) => (
                                            <div className="buttonInContent incomingMessage" key={i}>
                                                <div className="textButtonInContent" onClick={() => this.selectId(item.id)}>Сообщение #{item.id}</div>
                                            </div>))}
                                        </div>
                                        <div className="settings">
                                            <div className="back" onClick={() => this.changeState('step', 0)}>back</div> 
                                        </div> 
                                    </React.Fragment>
                                : null}
                                {this.state.step === 2 ?
                                    <React.Fragment>
                                        <div className="incomingMessageList">
                                            {this.state.inComingMessages.map((item, i) => (
                                            item.id === this.state.selectId ?
                                            <React.Fragment>
                                                <div className="headerContacts">Входящее от {item.num}</div>
                                                <div className="messageBy" style={{paddingLeft: '5px', paddingRight: '5px'}}>
                                                    <span style={{wordWrap: 'break-word'}}>{item.text}</span>
                                                </div>
                                            </React.Fragment>
                                            : null))}
                                        </div>
                                        <div className="settings">
                                            <div className="back" onClick={() => this.changeState('step', 1)}>back</div> 
                                        </div> 
                                    </React.Fragment>
                                : null}
                                {this.state.step === 3 ?
                                    <React.Fragment>
                                        <div className="outgoingMessageList">
                                            <div className="headerContacts">Исходящие</div>
                                            {this.state.outComingMessages.map((item, i) => (
                                            <div className="buttonInContent outgoingMessage" key={i}>
                                                <div className="textButtonInContent" onClick={() => this.selectId(item.id)}>Сообщение #{item.id}</div>
                                            </div>))}
                                        </div>
                                        <div className="settings">
                                            <div className="back" onClick={() => this.changeState('step', 0)}>back</div> 
                                        </div> 
                                    </React.Fragment>
                                : null}
                                {this.state.step === 4 ?
                                    <React.Fragment>
                                        <div className="outgoingMessageList">
                                            {this.state.outComingMessages.map((item, i) => (
                                            item.id === this.state.selectId ?
                                            <React.Fragment>
                                            <div className="headerContacts">Исходящее к {item.num}</div>
                                            <div className="messageBy" style={{paddingLeft: '5px', paddingRight: '5px'}}>
                                                <span style={{wordWrap: 'break-word'}}>{item.text}</span>
                                            </div>
                                            </React.Fragment>
                                            : null))}
                                        </div> 
                                        <div className="settings">
                                            <div className="back" onClick={() => this.changeState('step', 3)}>back</div> 
                                        </div> 
                                    </React.Fragment>
                                : null}                                                                                                                                                                                                                   
                            </React.Fragment>
                        : null}
                        {this.state.page === 3 ?
                            <React.Fragment>
                                {this.state.step === 0 ? 
                                    <React.Fragment>
                                        <div className="contactsList">
                                            <div className="headerContacts">Cписок контактов</div>
                                            <div className="contactAdd">
                                                <div className="textButtonInContent" onClick={() => this.changeState('step', 1)}>+ Добавить контакт</div>
                                            </div>
                                            {this.state.contacts.map((item, i) => (
                                            <div className="contact" key={i}>
                                                <div className="user" onClick={() => this.selectIdContact(item.id)}>{item.name}</div>
                                            </div>))}
                                        </div>
                                        <div className="settings">
                                            <div className="back" onClick={() => this.changeState('page', 0)}>back</div>
                                        </div>
                                    </React.Fragment>
                                : null}
                                {this.state.step === 1 ?
                                    <React.Fragment>
                                        <div className="addContact">
                                            <div className="headerContacts">Создание контакта</div>
                                            <form onSubmit={this.addContact}>
                                                <input id="contactName" tabIndex="-1" pattern="[A-Za-zА-Яа-яЁё]" type="text" name="contactName" maxLength="32" value={this.state.contactName} onChange={this.onChange} placeHolder="Имя"/>
                                                <input id="contactNumber" tabIndex="-1" type="text" name="contactNumber" style={{marginTop: '2.5px'}} maxLength="8" value={this.state.contactNumber} onChange={this.onChange} placeHolder="Номер"/>
                                            </form>
                                        </div> 
                                        <div className="settings">
                                            <div className="call" onClick={this.addContact}>SELECT</div>
                                            <div className="back" onClick={() => this.changeState('step', 0)}>back</div>
                                        </div>
                                    </React.Fragment>
                                : null}
                                {this.state.step === 2 ?
                                    <React.Fragment>
                                        <div className="options">
                                            <div className="headerContacts">Действия</div>
                                            {this.state.contacts.map((item, i) => (
                                                item.id === this.state.selectIdContact ?
                                                <div className="buttonInContent">
                                                    <div className="textButtonInContent" onClick={() => this.callContact(item.num)}>Позвонить</div>
                                                </div> : null
                                            ))}
                                            <div className="buttonInContent">
                                                <div className="textButtonInContent" style={{fontSize: '15px'}} onClick={() => this.changeState('step', 3)}>Написать сообщение</div>
                                            </div>
                                            <div className="buttonInContent">
                                                <div className="textButtonInContent" style={{fontSize: '15px'}} onClick={() => this.changeState('step', 4)}>Изменить</div>
                                            </div>
                                            <div className="buttonInContent">
                                                <div className="textButtonInContent" style={{fontSize: '15px'}} onClick={() => this.deleteContact()}>Удалить</div>
                                            </div>
                                        </div>
                                        <div className="settings">
                                            <div className="back" onClick={() => this.changeState('step', 0)}>back</div>
                                        </div>
                                    </React.Fragment>
                                : null}
                                {this.state.step === 3 ?
                                    <React.Fragment>
                                        <div className="message">
                                            <textarea maxLength="120" id="message" name="message" value={this.state.message} onChange={this.onChange}></textarea>
                                        </div> 
                                        <div className="settings">
                                            <div className="call" onClick={this.sendMessage}>SELECT</div>
                                            <div className="back" onClick={() => this.changeState('step', 2)}>back</div>
                                        </div>
                                    </React.Fragment>
                                : null}
                                {this.state.step === 4 ? 
                                    <React.Fragment>
                                        <div className="changeContact">
                                            <div className="headerContacts">Редактирование</div>
                                            <form onSubmit={this.changeContact}>
                                                {this.state.contacts.map((item, i) => (
                                                item.id === this.state.selectIdContact ?
                                                    <React.Fragment>
                                                        <input id="contactName" tabIndex="-1" pattern="[A-Za-zА-Яа-яЁё]" type="text" name="contactName" maxLength="32" value={this.state.contactName} onChange={this.onChange} placeHolder={item.name}/>
                                                        <input id="contactNumber" tabIndex="-1" type="text" name="contactNumber" style={{marginTop: '2.5px'}} maxLength="6" value={this.state.contactNumber} onChange={this.onChange} placeHolder={item.num}/>
                                                    </React.Fragment>
                                                : null))}
                                            </form>
                                        </div>
                                        <div className="settings">
                                            <div className="call" onClick={this.changeContact}>SELECT</div>
                                            <div className="back" onClick={() => this.changeState('step', 2)}>back</div>
                                        </div>
                                    </React.Fragment>
                                : null}
                            </React.Fragment>
                         : null}
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {

    };
};

const connected = connect(mapStateToProps)(TelePhone);
export { connected as TelePhone }; 