var RAGE = {};

RAGE.CallClient = (eventName, ...args) => { 
    if(window.mp){
        window.mp.trigger(eventName, ...args);
        console.log(`[TRIGGER DEBUG] Trigged: ${eventName} - ${args}`);
    }
    else console.log(`[TRIGGER DEBUG] Trigged: ${eventName} - ${args}`);
}

RAGE.CallServerCommand = (cmd) => { 
    if(window.mp) window.mp.invoke("command", cmd);
    else console.log(`[INVOKE DEBUG] Command to server: ${cmd}`);
}

RAGE.InvokeFocus = (toggle) => { 
    if(window.mp) mp.invoke('focus', toggle)
    else console.log(`[INVOKE FOCUS DEBUG] Toggle: ${toggle}`);
}

export default RAGE;